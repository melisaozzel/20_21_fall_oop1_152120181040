#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
	/* Enter your code here. Read input from STDIN. Print output to STDOUT */
	int n, q, k, a, b, i, j;

	cin >> n >> q;
	vector<vector<int>>ar(n);

	for (i = 0; i < n; ++i) {
		cin >> k;
		ar[i].resize(k);
		for (j = 0; j < k; ++j) {
			cin >> ar[i][j];
		}
	}

	for (int M = 0; M < q; ++M) {
		cin >> a >> b;
		cout << ar[a][b] << endl;
	}

	
	system("pause");
}