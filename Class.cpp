#include <iostream>
#include <sstream>
using namespace std;

class Student {

public:
	int newage;
	int newstandard;
	string first_name2;
	string last_name2;


	void set_age(int age) {
		newage = age;
	}
	void set_standard(int standard) {
		newstandard = standard;
	}
	void set_first_name(string firstname) {
		first_name2 = firstname;
	}
	void set_last_name(string lastname) {
		last_name2 = lastname;
	}

	int get_age() {
		return newage;
	}
	int get_standard() {
		return newstandard;
	}
	string get_first_name() {
		return first_name2;
	}
	string get_last_name() {
		return last_name2;
	}

	string to_string() {
		stringstream x;
		string a;


		x << newage << "," << first_name2 << "," << last_name2 << "," << newstandard;
		a = x.str();
		return a;
	}

};

int main() {
	int age, standard;
	string first_name, last_name;

	cin >> age >> first_name >> last_name >> standard;

	Student st;
	st.set_age(age);
	st.set_standard(standard);
	st.set_first_name(first_name);
	st.set_last_name(last_name);

	cout << st.get_age() << "\n";
	cout << st.get_last_name() << ", " << st.get_first_name() << "\n";
	cout << st.get_standard() << "\n";
	cout << "\n";
	cout << st.to_string();

	return 0;
}